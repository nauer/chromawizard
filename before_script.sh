#!/bin/bash

# PIP Dependencies
# pyqt5 and cv2 dependencies
apt-get update --fix-missing && apt-get install -y wget bzip2 ca-certificates libglib2.0-0 libxext6 libsm6 libxrender1 git mercurial subversion libgl1-mesa-glx libgtk2.0-0

pip3 install wheel
pip3 install pyqt5
pip3 install opencv-python
pip3 install twine

# Conda Dependencies
# Miniconda pyckage builder
echo 'export PATH=/opt/conda/bin:$PATH' > /etc/profile.d/conda.sh && wget --quiet https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && /bin/bash ~/miniconda.sh -b -p /opt/conda && rm ~/miniconda.sh
echo 'export PATH=/opt/conda/bin:$PATH' >> .bashrc

PATH=/opt/conda/bin:$PATH

# Conda build
conda install -yq conda-build
conda config --add channels menpo
conda config --add channels anaconda
conda create -yqn chroma -c menpo python=3.6 pyqt opencv3
