# ChromaWizard - Boost your karyotyping workflow

Multicolour image analysis finds its application in a broad range of biological studies,
including multiplex-fluorescence in-situ hybridization (M-FISH), which is able to
facilitate the analysis of individual chromosomes or segments of chromosome in
complex metaphase spreads (chromosome painting). These techniques are used to
study and identify numerical and structural chromosome aberrations as observed in
many immortalised and cancer cell lines. Currently available image analysis software
is mostly offered as a proprietary, expensive software in combination with hardware
bundles. These applications are fast and powerful, however, they lack inter-
compatibility since they are mostly developed for a specific model organism or
hardware system and are not free to use for research purposes. Thus, open software could pave the way for flexible and versatile tools with customizable functionalities
depending upon the application needs. To this end we have developed an open source
tool for analysing M-FISH based images on the excellent SciPy and OpenCV
libraries called "ChromaWizard". We demonstrate functionalities of this tool using
Chinese hamster (*Cricetulus griseus*) and CHO-K1 karyotype paintings with
commercially available 12XCHamster (MetaSystems) labeling. **"ChromaWizard"** is
functional and versatile, since it allows the direct inspection of the original
hybridization signals and enables the user to assign signals semi-automatically. Finally,
the tool is easily customizable and can be further developed for other multicolour
image analysis applications.

## Installation

### Linux & MacOS

~~~ .bash
# Install ChromaWizard
sudo pip3 install chromawizard
~~~

### Microsoft Windows
ChromaWizard is also available as Conda package. Install Conda for your operating system and type:

~~~ .bash
conda install -c nauer chromawizard
~~~

## Tutorial
* [![ChromaWizard Tutorial 1](https://gitlab.com/nauer/chromawizard/tree/master/Tutorial_1/acib_splash.png)](https://gitlab.com/nauer/chromawizard/blob/master/Tutorial_1/Tutorial_1.2.24.mp4)
