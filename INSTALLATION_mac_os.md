# Create Wheel Package
python3 setup.py bdist_wheel -d chroma


pip3 install numpy
pip3 install PyQt5
pip3 install opencv-python

# The attached program
pip3 install chromawizard-1.1.4-py3-none-any.whl

# Run it
chromawizard

Python3 and pip3 must already be installed.


