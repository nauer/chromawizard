# ChromaWizard

## Importing and Converting
Load image in grayscale mode. `imread` converts image to a **numpy** `ndarray` automatically.

~~~python
cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
~~~

## Masking
Create a simply binary image mask to differ between background and foreground with a threshold value. It uses *Otsu's* method to get a good starting value.
There are various masks. The general threshold mask is set by the Otsu's method or a manual threshold is set by a slider. The positive and negative mask are set manually by the user. All masks are bit-wise logically interconnected to a global mask.

~~~python
cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
~~~

## Segmentation, Filtering and Counting
Chromosomes are segmented by the `connectedComponents` function. It connects all adjacent pixels from the binary image not background and mark each pixel with the same label for each distinct group.
Single chromosomes can be selected by only testing for the specific label. The resulting groups are counted. Dependent on the pixel size of each group/chromosome small artifacts can be filtered out.

~~~python
cv2.connectedComponents(image, cv2.CV_16U)
~~~

## Chromosome Rotating and Cutting
Segmented Chromosomes are cut out and rotated in a vertically position.

~~~python
cv2.warpAffine(image, matrix, resolution)
~~~

## Chromosome Coloring
Each loaded channel is assigned to a value in a bit mask. The combinations of these channels are assigned to a user configurable chromosome color.

## Used Libraries

### Image Converting
* **opencv2**
* **scikit-image** (only a 3 functions - not imported, copied from sources and adapted)
* **NumPy**

### Graphical User Interface
* Qt5


### Bibtex
**opencv2**
~~~
@article{opencv_library,
    author = {Bradski, G.},
    citeulike-article-id = {2236121},
    journal = {Dr. Dobb's Journal of Software Tools},
    keywords = {bibtex-import},
    posted-at = {2008-01-15 19:21:54},
    priority = {4},
    title = ,
    year = {2000}
}
~~~

**scikit-image**
~~~
@article{scikit-image,
 title = {scikit-image: image processing in {P}ython},
 author = {van der Walt, {S}t\'efan and {S}ch\"onberger, {J}ohannes {L}. and
           {Nunez-Iglesias}, {J}uan and {B}oulogne, {F}ran\c{c}ois and {W}arner,
           {J}oshua {D}. and {Y}ager, {N}eil and {G}ouillart, {E}mmanuelle and
           {Y}u, {T}ony and the scikit-image contributors},
 year = {2014},
 month = {6},
 keywords = {Image processing, Reproducible research, Education,
             Visualization, Open source, Python, Scientific programming},
 volume = {2},
 pages = {e453},
 journal = {PeerJ},
 issn = {2167-8359},
 url = {http://dx.doi.org/10.7717/peerj.453},
 doi = {10.7717/peerj.453}
}
~~~

**NumPy**
~~~
@article{van_der_walt_numpy_2011,
        title = {The {NumPy} {Array}: {A} {Structure} for {Efficient} {Numerical} {Computation}},
        volume = {13},
        issn = {1521-9615},
        shorttitle = {The {NumPy} {Array}},
        url = {http://ieeexplore.ieee.org/document/5725236/},
        doi = {10.1109/MCSE.2011.37},
        number = {2},
        urldate = {2017-02-01},
        journal = {Computing in Science \& Engineering},
        author = {van der Walt, Stéfan and Colbert, S Chris and Varoquaux, Gaël},
        month = mar,
        year = {2011},
        pages = {22--30}
}
~~~
