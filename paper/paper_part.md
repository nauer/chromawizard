## Abstract (Max.200).
Multicolor image analysis finds its applications in a broad range of biological studies. Specifically, multiplex-fluorescence in situ hybridization (M-FISH), facilitate analysis of individual chromosomes or segments of chromosomes in complex metaphase. M-FISH is a widely used technique in both numerical and structural chromosome aberrations. Current available software is propitiatory bundled with hardware and lack of inter-compatibility limiting application to few spices. Thus, open source software could pave the way for a flexible and versatile tool with customizable functionalities. We have developed an open source tool for M-FISH analysis called as "ChromaWizard". The developed graphical application using popular scientific image analysis libraries (OpenCV, scikit-image and NumPy). We demonstrate functionalities of the tool using Chinese hamster (Cricetulus griseus) and CHO cell lines karyotype paintings labeled by a commercially available probe kit. The application allows direct inspection of the original hybridization signals and assigning signals either manually or automatically making it to a very functional and versatile software. Finally, the application can be easily adapted and expanded for other multicolor image analysis applications without any limitations to the hardware and supports a wide varieties of protocols.

## Keywords
karyotype, chromosome-painting, M-FISH

### Pre-analysis configuration and customization
#### Installation
##### Linux
ChromaWizard can be easily installed on Linux systems using the Python Package Index (PyPI). (after publishing)

~~~bash
# For system wide installation
pip install ChromaWizard

# For local installation without administrative privileges
pip --user install ChromaWizard
~~~
##### Windows
A convenient Anaconda package for Windows user is in preparation.

#### Configuration
ChromaWizard is highly configurable and can be adapted to any kind of organism and labeling kit. ChromaWizard creates a hidden configuration file in the user's home folder after the first run, automatically. The default configuration is set up for the 6 channel 12XCHamster labeling kit from Metasystem. Number and names of the channels both and chromosome association can be set in the configuration file. The configuration file is a simple text file in JSON (JavaScript Object Notation for abbreviations) format. A JSON editor like http://www.jsoneditoronline.org/ for changing the file would be recommended but is not obligatory. A standard text editor would be sufficed.

##### Channels
Channels are represented by/as a bit mask. For each channel a position in the mask is set. Positions start from 1 and every extra channel suffix a 0 to the previous channel. All positions must be filled from beginning to the end. The channel name is associated to each position number as shown in Listing 1. The first channel with position 1 is obligatory and is used as main mask for the chromosome segmentation. It is usually used as DAPI channel. This makes it quiet easy to add or remove a channel depending on the used labeling kit.

###### Listing  1:
~~~json
{
  "channel_names": {
    "1": "DAPI",
    "10": "Aqua",
    "100": "Green",
    "1000": "Orange",
    "10000": "Red",
    "100000": "NIR"
  }
}


~~~

##### Chromosome Association
All labeling kits use a set of dyes and its combination for identifying single chromosomes. Diagram 1 shows the labeling scheme for the **12XCHamster** kit.

![alt](../presentation/reveal.js/img/12XCHamster.jpg)

Listing 2 shows the configuration for the *Hamster* chromosome 4. The bitmask **1010** defines that chromosome 4 is a combination of channel 2 (Aqua 10) and channel 4 (Orange 1000). By combining channels in the bitmask all combinations of the used dyes are possible.

###### Listing  2:
~~~json
"chromosomes": {
    "1010": {
      "color": "#EF2929",
      "name": "Chr 4",
      "order": 4
    },
}
~~~
The 'color' field defines the chromosome color shown in the false color representation used in the mask windows and karyotype view. As value format standard HTML color code is used. The field 'name' and the field 'order' define the name and chromosome order in the legend dialog.
