from chroma import main as m


def main():
    return m.main()


if __name__ == "__main__":
    exit(main())
