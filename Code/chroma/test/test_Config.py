import unittest
import sys
from pathlib import Path

# `path.parents[1]` is the same as `path.parent.parent`
dir_path = str(Path(__file__).resolve().parents[2])
sys.path.insert(0, dir_path)

from chroma import Config

# Run test from outside like this
# python3 -m unittest discover -v -p "test_Co*"  - default pattern is "test*.py"

class TestData(unittest.TestCase):
    def test_data_creation(self):
        d = Config.Data({'a':5, 'b':"DATA"})
        self.assertEqual(d.a, 5)
        self.assertEqual(d.b, "DATA")

if __name__ == '__main__':
    unittest.main()
