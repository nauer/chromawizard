import unittest
import sys
from pathlib import Path

import numpy as np

# `path.parents[1]` is the same as `path.parent.parent`
dir_path = str(Path(__file__).resolve().parents[2])
sys.path.insert(0, dir_path)

from chroma.Utils import (Utils)

# Run test from outside like this
# python3 -m unittest discover -v -p "test_Co*"  - default pattern is "test*.py"

class TestUtils(unittest.TestCase):
    def test_get_empty_color_image_3(self):
        data = Utils.get_empty_color_image_3((5, 5))

        self.assertEqual(data.all(), np.zeros((5,5,3), dtype=np.uint8).all())

if __name__ == '__main__':
    unittest.main()