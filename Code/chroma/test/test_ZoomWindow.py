import unittest
import sys

# Add search path chroma that absolute path will work
sys.path.insert(0, '../..')
from chroma import ZoomWindow

# Run test from outside like this
# python3 -m unittest discover -v -p "test_Co*"  - default pattern is "test*.py"

if __name__ == '__main__':
    unittest.main()
