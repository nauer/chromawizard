import unittest
import sys
from pathlib import Path

# Add search path chroma that absolute path will work
# `path.parents[1]` is the same as `path.parent.parent`
dir_path = str(Path(__file__).resolve().parents[2])
sys.path.insert(0, dir_path)
from chroma import Cube

# Run test from outside like this
# python3 -m unittest discover -v -p "test_Co*"  - default pattern is "test*.py"

if __name__ == '__main__':
    unittest.main()
