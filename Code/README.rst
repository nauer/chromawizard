Chromawizard 1.2.0
=======================
A tool for creating karyotypes of M-FISH images.

Installation
=======================
Download or clone Chromawizard:
::

    cd chromawizard/Code
    git checkout v1.1.8-release
    pip3 install .

or directly from PyPI:
::

    pip3 install chromawizard

A Conda package for Windows user is also available:
::

    conda install -c nauer chromawizard

Run ChromaWizard
=======================
::

    chromawizard

or in debug mode:
::

    chromawizard -l Debug
