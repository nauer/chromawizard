#!/bin/bash

python3 -c "from PyQt5.QtCore import QT_VERSION_STR; print(QT_VERSION_STR)"
python3 -c "from PyQt5.QtCore import QLocale"

cd Code
python3 setup.py bdist_wheel
#python3 setup.py sdist bdist_wheel

twine upload dist/* --config-file .pypirc -u $PYPI_USER -p $PYPI_PASSWORD
